(ns marquetry.core-test
  (:require [clojure.test :refer :all]
            [marquetry.core :refer :all]))

(deftest math
  (testing "Math"
    (is (= 2 (render ">(+ 1 1)<")))
    (is (= 2.5 (render ">(+ 1.5 1)<")))
    (is (= -1  (render ">(+ -2 1)<")))

    (is (= 4 (render ">(* 2 2)<")))
    (is (= 21.375 (render ">(* 4.75 4.5)<")))
    (is (= 4  (render ">(* -2 -2)<")))
    (is (= -2  (render ">(* -2 1)<")))

    (is (= 2  (render ">(/ 4 2)<")))
    (is (= -2  (render ">(/ -4 2)<")))
    (is (= 2  (render ">(/ 4 2)<")))
    (is (thrown-with-msg? Exception #"Divide by zero"
                          (render ">(/ 1 0)<")))

    (is (= 0 (render ">(- -1 -1)<")))
    (is (= -3 (render ">(- -2 1)<")))))

(deftest string
  (testing "String Functions")
  (is (= "joinedstring" (render ">(str \"joined\" \"string\")<")))
  (is (= "joinedstring" (render ">(format \"%s%s\" \"joined\" \"string\")<"))))

(deftest logical
  (testing "Logical Operators")

  (is (= true (render ">(= 1 1)<")))
  (is (= false (render ">(not (= 1 1))<")))
  (is (= true (render ">(> 2 1)<")))
  (is (= true (render ">(< 1 2)<"))))

(deftest control
  (testing "Flow Control Functions"
    (is (= true (render ">(if (= 1 1) true false)<")))
    (is (= false (render ">(if (not (= 1 1)) true false)<")))))

(deftest functions
  (testing "Functions"
    (is (= "foo:bar"
           (render ">(do (defn test [a b] (str a \":\" b)) \"\") (test \"foo\" \"bar\")<")))
    (is (= "foo:bar"
           (render ">(test \"foo\" \"bar\")<" :data-map {:test (fn [a b] (str a ":" b))})))))
