(ns marquetry.core
  "Marquetry is an ill-conceived string templating library."
  (:require [sci.core :refer [eval-string init]]
            [clojure.string :as s]))

(defn- level-token?
  "Check if the input token and level match the context."
  [token lvl {:keys [input level]}]
  (every? true? (conj (map = token input) (= level lvl))))

(defn- level->
  "Generalized transform function. Takes a level keyword and transforms the
  supplied context to the new level."
  [k {:keys [level parse input output] :as ctx}]
  (merge ctx
         {:level k
          :parse []
          :output (if (not (empty? parse))
                    (conj output [level (apply str parse)])
                    output)
          :input (drop 2 input)}))

(defn- parser
  "Parse the string and return a simple AST."
  [s]
  (loop [ctx {:level :text :parse [] :output [] :input (seq s)}]
    (cond
      ;; If there is no more input return the output map
      (empty? (:input ctx)) (if (empty? (:parse ctx))
                              (:output ctx)
                              (conj (:output ctx)
                                    [(:level ctx)
                                     (apply str (:parse ctx))]))
      ;; Opening Value Tag
      (level-token? "<(" :text ctx) (recur (level-> :value ctx))
      ;; Closing Value Tag
      (level-token? ")>" :value ctx) (recur (level-> :text ctx))
      ;; Opening Clojure Tag
      (level-token? ">(" :text ctx) (recur (level-> :clojure ctx))
      ;; Closing Clojure Tag
      (level-token? ")<" :clojure ctx) (recur (level-> :text ctx))
      ;; If nothing matches take next item off of the input
      :else (recur (let [{:keys [input parse] :as ctx} ctx]
                     (merge ctx
                            {:parse (conj parse (first input))
                             :input (rest input)}))))))

(defn- transform
  "Transform a parsed AST."
  [{:keys [data-map sci-env namespace] :as ctx
    :or {sci-env (atom {}) data-map {} namespace 'user}}
   ast]
  (let [e {:namespaces {namespace (into {} (for [[k v] data-map]
                                             [(symbol k) v]))}
           :env sci-env}]
    (map (fn [[k v]]
           (case k
             :text v
             :clojure (eval-string (str "(" v ")") e)
             :value (get-in data-map (map keyword (s/split v #"\s")))))
         ast)))

(defn render
  "Takes a marquetry template string and an optional map which can
  supply which takes the optional values :sci-env and :data-map.

  template can be a string or a java.io.File object. Strings are
  templated directly files are read into memory and then templated.

  :sci-env should be an atom with a map. SCI will use this to store
  its environment allowing us to use the same environment between
  different render runs.

  :data-map which is a map of the data to be used in our marquetry
  template. All first level keys in the map will be interned into the
  SCI user namespace as symbols with their values bound. This makes
  them avaliable to use as variables in our templates.

  Templates can contain any text except text between the tokens <()>
  and >()< are treated specially.

  <()> is for variable substitution. Between <()> is a list of space
  separated identifiers. These will be converted to keywords and
  looked up in the supplied data-map and replaced with the found value.

  >()< is for code evaluation. >()< is equivalent to () in
  clojure. The code between the >()< will be interpreted with the SCI
  interpreter and replaced with the result. The keywords in the
  supplied :data-map are interned into the SCI user namespace as
  symbols with bound values. This makes them available as variables in
  the >()< blocks but has the side effect of overriding any functions
  that have the same name as supplied keywords. If this behavior is
  undesierable a specific namespace can be passed in the argument map
  with the keyword :namespace.
  "
  [template & {:keys [data-map sci-env namespace] :as ctx
               :or {sci-env (atom {}) data-map {} namespace 'user}}]
  (->> #?(:clj (if (= java.io.File (type template))
                 (slurp template) ; Hope we aren't templating a large file....
                 template)
          ;; In clojurescript lets stick to templating strings.
          :cljs (template))
       (parser)
       (transform ctx)
       ((fn [templated]
          (if (empty? (rest templated))
            ;; When there is a single result return it directly.
            (first templated)
            ;; Concatenate multiple results into a single string.
            (apply str templated))))))
