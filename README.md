# Marquetry

A clojure templating system using [SCI](https://github.com/babashka/sci) for embedded expressions.

[![Marquetry is a woodworking technique.](marquetry.jpg)](https://www.flickr.com/photos/paullew/32447988493/)

## Usage

Add the following dependency to your `project.clj` file

[![Clojars Project](http://clojars.org/marquetry/latest-version.svg)](http://clojars.org/marquetry)

## Getting Started

``` clojure
(require '[marquetry.core :as m])
(m/render "Templated: <(foo)>" {:foo "bar"})
```

Returns:

```
"Templated: bar"
```

## Usage

The Clojure interface consists only one function, the `render` function. This
function takes a string and an optional map and returns the string templated
according to the rules of the templating DSL.

### Clojure API

#### render
``` clojure
[s & [m]]
```

- `s` is the string to be templated.
- `m` is a map of values available to the template. First level keys will be 
    interned as symbols in the SCI namespace.

Returns the result of SCI evaluation. If the result is a single object that
object is returned. If the result is multiple objects then they are
concatenated into a single string and that string is returned.

### DSL API

The DSL embeds the SCI interpreter.

Space separated identifiers inside <()> will be replaced with the value of the
key in the supplied map.

Code inside >()< will be run as clojure code using SCI and replaced with the
result of the evaluation.

The map supplied to the render function will have all first level keys
interned as symbols in the SCI user namespace. Nested map keys are left
unchanged. So a map like `{:foo {:bar "BAZ"} :bar "BAZ"}` will appear in the
SCI namespace as two variables `foo` with the value `{:bar "BAZ"}` and `bar`
with the value `"BAZ"`.

## License

Copyright © 2021 Jeffrey Stoffers

Distributed under the Eclipse Public License either version 1.0 or (at your
option) any later version.
