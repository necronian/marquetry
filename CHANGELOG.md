# Change Log

## 1.0.0 - 2021-12-27
### Changed
- Replaced simple lispy language with [SCI](https://github.com/babashka/SCI)
  this is a large breaking change.
- Moved from lein to deps.edn

## 0.2.1 - 2018-12-07
### Changed
- Fixed failing tests.

## 0.2.0 - 2018-12-06
### Changed
- Namespaced marquetry internal keys so they don't interfere with the user.
### Added
- <()> does string templating and >()< templating evaluates to clojure values.
- Several new functions upper, replace, capitalize, when, or, and, first, rest

## 0.1.0 - 2018-06-08
### Added
- Initial release
